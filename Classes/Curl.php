<?php

namespace Classes;

/**
 * @method $this option(int $option, mixed $value) 自定义curl选项参数
 * @method $this autoCookie(bool $autoCookie = false) 启用 cookie自动化
 * @method $this timeout(int $timeout = 5) 请求超时秒数
 * @method $this location(bool|int $location = true) 启用自动重定向或指定最大重定向次数
 * @method $this get(string $url, string|array $params = null, string $custom_request = null) 发起get请求
 * @method $this upload(string $url, string|array $params = null, string|null $custom_request = null) 发起上传文件请求
 * @method $this post(string $url, string|array $params = null, string $custom_request = null) 发起post请求
 * @method $this json(string $url, string|array $params = null, string $custom_request = null) 发起json请求
 * @method $this ua(string $ua):Curl 请求头：User-Agent
 * @method $this referer(string|bool $referer) 请求头：Referer
 * @method $this header(string|array $key, string $value = null) 其它请求头
 * @method $this addHeader(string|array $key, string $value = null) 其它请求头
 * @method $this cookie(string|array $name, string $value = null) 自定义请求cookie
 * @method $this addCookie(string|array $name, string $value = null) 自定义请求cookie
 * @method $this ajax(string|bool $ajax = true) 请求头：X-Requested-With
 * @method array getCookie(string $url = null) 获取cookie_jar存储的cookie或请求返回的cookie（依据是否开启cookie自动化）
 * @method array|string head(bool $array = true) 获取响应头
 * @method string html(string $charset = null) 获取响应html文本
 * @method mixed obj(bool $associative = false) 获取响应json对象
 * @method \SimpleXMLElement xml(bool $json = null, int $xml_option = null) 获取响应simplexml对象
 * @method string text(string $charset = null) 获取响应内容
 * @method string body(string $charset = null) 获取响应内容
 * @method string response(string $charset = null) 获取响应内容
 * @method array|mixed all(string $key = null) 所有curl数据
 * @method string redirect_url() 获取重定向地址
 * @method bool|string down(string $file = null, string $name = null) 下载文件到指定位置
 */
class Curl {
	const ua_android = 'Mozilla/5.0 (Linux; Android 10.0; MHA-AL00 Build/HUAWEIMHA-AL00; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/43.0.2357.134 Mobile Safari/537.36';
	const ua_iphone  = 'bili-universal/65600100 CFNetwork/1240.0.4 Darwin/20.6.0 os/ios model/iPhone X mobi_app/iphone build/65600100 osVer/14.7.1 network/2 channel/AppStore';
	const ua_pc      = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36 Edg/89.0.774.54';
	const ua_applet  = 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36 MicroMessenger/7.0.9.501 NetType/WIFI MiniProgramEnv/Windows WindowsWechat';

	private $cookie_jar;
	public $options = [
		CURLOPT_HEADER         => true,                  // 输出响应头
		CURLOPT_TIMEOUT        => 5,                     // 超时(秒)
		CURLOPT_RETURNTRANSFER => true,                  // 输出数据流
		CURLOPT_FOLLOWLOCATION => true,                  // 自动跳转追踪
		// CURLOPT_MAXREDIRS => 2,
		CURLOPT_AUTOREFERER    => true,                  // 自动设置来路信息
		CURLOPT_SSL_VERIFYPEER => false,                 // 认证证书检查
		CURLOPT_SSL_VERIFYHOST => false,                 // 检查SSL加密算法
		CURLOPT_NOSIGNAL       => false,                 // 忽略所有传递的信号
		CURLOPT_HTTPHEADER     => [],                    // 请求头
		CURLINFO_HEADER_OUT    => true,                  // 获取请求头
		CURLOPT_ENCODING       => 'gzip, deflate, br'
	];

	private function __construct () {
		$this->cookie_jar = $this->charset(__DIR__, "GBK") . DIRECTORY_SEPARATOR . 'cookie.txt';
	}

	/**
	 * 初始化
	 *
	 * @return \Classes\Curl
	 */
	public static function init () {
		return new self();
	}

	public function __call ($method, $args) {
		switch ($method) {
			case 'option':
				if (isset($args[0])) {
					if (is_array($args[0]))
						foreach ($args[0] as $k => $v) {
							if ($k === CURLOPT_CUSTOMREQUEST) {
								unset($this->options[CURLOPT_POST]);
								$v = strtoupper($v);
							}
							$this->options[$k] = $v;
						}
					else if (isset($args[1])) {
						if ($args[0] === CURLOPT_CUSTOMREQUEST) {
							unset($this->options[CURLOPT_POST]);
							$args[1] = strtoupper($args[1]);
						}
						$this->options[$args[0]] = $args[1];
					}
				}
				break;
			case 'autoCookie': // Cookie自动化，默认关闭
				if (@$args[0] === true) {
					$this->options[CURLOPT_COOKIEFILE] = $this->cookie_jar;
					$this->options[CURLOPT_COOKIEJAR]  = $this->cookie_jar;
				}
				break;
			case 'timeout':
				if (isset($args[0]))
					$this->options[CURLOPT_TIMEOUT] = $args[0];
				break;
			case 'location': // 自动重定向，默认开启
				if (isset($args[0])) {
					$this->options[CURLOPT_FOLLOWLOCATION] = boolval($args[0]);
					if (@ctype_digit($args[0]) && $args[0] > 0) $this->options[CURLOPT_MAXREDIRS] = $args[0];
				}
				break;
			case 'get':
				if (isset($args[0])) {
					$data                       = isset($args[1]) ? is_array($args[1]) ? http_build_query($args[1], '', '&', PHP_QUERY_RFC3986) : $args[1] : false;
					$this->options[CURLOPT_URL] = $data !== false ? $args[0] . (preg_match('/\?/', $args[0]) ? '&' : '?') . $data : $args[0];
				}
				break;
			case 'upload':
				if (isset($args[0])) {
					$this->options[CURLOPT_URL] = $args[0];
					if (isset($args[2]))
						$this->options[CURLOPT_CUSTOMREQUEST] = strtoupper($args[2]);
					else
						$this->options[CURLOPT_POST] = true;
					if (isset($args[1]))
						$this->options[CURLOPT_POSTFIELDS] = $args[1];
				}
				break;
			case 'post':
				if (isset($args[0])) {
					$this->options[CURLOPT_URL] = $args[0];
					if (isset($args[2]))
						$this->options[CURLOPT_CUSTOMREQUEST] = strtoupper($args[2]);
					else
						$this->options[CURLOPT_POST] = true;
					if (isset($args[1]))
						$this->options[CURLOPT_POSTFIELDS] = is_array($args[1]) ? http_build_query($args[1], '', '&', PHP_QUERY_RFC3986) : $args[1];
				}
				break;
			case 'json':
				if (isset($args[0])) {
					$this->options[CURLOPT_URL] = $args[0];
					if (isset($args[2]))
						$this->options[CURLOPT_CUSTOMREQUEST] = strtoupper($args[2]);
					else
						$this->options[CURLOPT_POST] = true;
					$this->options[CURLOPT_HTTPHEADER][] = 'Content-Type: application/json; charset=utf-8';
					if (!empty(@$args[1]))
						$this->options[CURLOPT_POSTFIELDS] = is_array($args[1]) ? json_encode($args[1], 256) : $args[1];
				}
				break;
			case 'ua':
				if (isset($args[0])) {
					if (in_array(strtolower($args[0]), ['m', 'wap', 'android']))
						$args[0] = self::ua_android;
					else if (strcasecmp($args[0], 'iphone') === 0)
						$args[0] = self::ua_iphone;
					else if (strcasecmp($args[0], 'pc') === 0)
						$args[0] = self::ua_pc;
					else if (strcasecmp($args[0], 'applet') === 0)
						$args[0] = self::ua_applet;
					else if (strcasecmp($args[0], 'auto') === 0)
						$args[0] = @$_SERVER['HTTP_USER_AGENT'];
					$this->options[CURLOPT_USERAGENT] = $args[0];
				}
				break;
			case 'referer': // 来路
				if (isset($args[0]))
					$this->options[CURLOPT_REFERER] = $args[0] === true && !empty(@$this->options[CURLOPT_URL]) ? $this->options[CURLOPT_URL] : $args[0];
				break;
			case 'header':
			case 'addHeader':
				if (isset($args[0])) {
					if (is_array($args[0])) { // 数组键值对形式
						foreach ($args[0] as $k => $v)
							$this->options[CURLOPT_HTTPHEADER][] = $k . ': ' . $v;
					} else if (isset($args[1])) { // 双参数键值形式
						$this->options[CURLOPT_HTTPHEADER][] = $args[0] . ': ' . $args[1];
					} else {// 单参数形式
						$arr = preg_match_all('/([^:]*?)\s*:\s*(.*)\s*/', $args[0], $m) > 0 ? array_combine($m[1], $m[2]) : [];
						foreach ($arr as $k => $v)
							$this->options[CURLOPT_HTTPHEADER][] = $k . ': ' . $v;
					}
				}
				break;
			case 'cookie':
			case 'addCookie':
				if (isset($args[0])) {
					if (is_array($args[0])) { // 数组键值对形式
						$arr = [];
						foreach ($args[0] as $k => $v)
							$arr[] = $k . '=' . $v;
						$cookie = implode('; ', $arr);
					} else if (isset($args[1])) // 双参数键值形式
						$cookie = $args[0] . '=' . $args[1] . ';';
					else // 单参数形式
						$cookie = $args[0];

					$this->options[CURLOPT_COOKIE] = !empty(@$this->options[CURLOPT_COOKIE]) ? $this->options[CURLOPT_COOKIE]
						. (preg_match('/;\s*$/', $this->options[CURLOPT_COOKIE]) ? '' : '; ') . $cookie : $cookie;
				}
				break;
			case 'ajax':
				if (isset($args[0]))
					$this->options[CURLOPT_HTTPHEADER][] = 'X-Requested-With: ' . ($args[0] === true ? 'XMLHttpRequest' : $args[0]);
				break;
			case 'getCookie': // 读取Cookie文件中的cookie
				if (empty(@$this->options[CURLOPT_COOKIEFILE]) || empty(@$this->options[CURLOPT_COOKIEJAR]))
					return $this->request($method, $args);

				$url    = isset($args[0]) && filter_var($args[0], FILTER_VALIDATE_URL) ? $args[0] : $this->options[CURLOPT_URL];
				$cookie = @file_get_contents($this->cookie_jar);
				$arr    = parse_url($url);
				$host   = preg_replace('/.*?\.(?=.+\..+$)/', '', $arr['host']);
				$p      = preg_match_all('/' . preg_quote($host, '/') . '(?:\s+([^\s\r\n]+))(?:\s+([^\s\r\n]+))(?:\s+([^\s\r\n]+))(?:\s+([^\s\r\n]+))(?:\s+([^\s\r\n]+))(?:\s+([^\s\r\n]+))(?:\r*\n|$)/', rawurldecode($cookie), $m);

				return $p > 0 ? array_combine($m[5], $m[6]) : [];
			case 'head': // 获取响应头，默认不需要响应体，false表示需要
				$this->options[CURLOPT_HEADER] = true;
				$this->options[CURLOPT_NOBODY] = true;

				return $this->request($method, $args);
			case 'html':
			case 'obj':
			case 'xml':
			case 'text':
			case 'body':
			case 'response':
			case 'all':
			case 'redirect_url':
				return $this->request($method, $args);
			case 'down':
				$this->options[CURLOPT_HEADER]         = false;
				$this->options[CURLOPT_NOBODY]         = false;
				$this->options[CURLOPT_FOLLOWLOCATION] = true;

				return $this->request($method, $args);
			default:
				exit("错误：不支持{$method}方法！");
		}

		return $this;
	}

	//执行请求(get, post)并返回数据
	private function request ($method, $args) {
		$ch = curl_init();
		curl_setopt_array($ch, $this->options);
		$data = curl_exec($ch);
		$info = curl_getinfo($ch);
		curl_close($ch);
		if ($method === 'down') {
			if (isset($args[1])) $file = trim($args[0]) . DIRECTORY_SEPARATOR . trim($args[1]);
			else if (isset($args[0])) $file = trim($args[0]);
			else $file = './' . pathinfo($this->options[CURLOPT_URL], PATHINFO_BASENAME);
			try {
				$fo = fopen($file, 'a');
				fwrite($fo, $data);
				fclose($fo);

				return true;
			} catch (\Exception $e) {
				return $e->getMessage();
			}
		}

		$info['header']   = trim(substr($data, 0, $info['header_size']));
		$info['response'] = substr($data, $info['header_size']);

		$headers = [$info['http_code']];
		$p       = preg_match_all('/(?<=^|[\r\n])\s*([^:]+)\s*:\s*(.+?)\s*(?=[\r\n]|$)/', $info['header'], $n);

		$info['headers'] = $p > 0 ? array_merge_recursive($headers, $this->array_combine_recursive($n[1], $n[2])) : [];

		$cookies         = isset($info['headers']['Set-Cookie']) ? $info['headers']['Set-Cookie'] : @$info['headers']['set-cookie'];
		$info['cookies'] = !empty($cookies) ? $this->parse_cookie(is_array($cookies) ? $cookies : [$cookies]) : [];
		$info['cookie']  = !empty($info['cookies']) ? array_column($info['cookies'], 'value', 'key') : [];

		$info['obj'] = ($obj = json_decode($info['response'])) && is_object($obj) ? $obj : null;

		$result = $info['response'];
		switch ($method) {
			case 'head':
				$result = $info[@$args[0] ? 'header' : 'headers'];
				break;
			case 'getCookie':
				$result = $info['cookie'];
				break;
			case 'text':
			case 'body':
			case 'response':
			case 'html':
				if (isset($args[0]))
					$result = $this->charset($result, $args[0]);
				if ($method === 'html')
					$result = html_entity_decode($result, ENT_HTML5);
				break;
			case 'obj':
				$result = json_decode($info['response'], @$args[0] === true);
				break;
			case 'xml':
				$result = simplexml_load_string($info['response'], 'SimpleXMLElement', isset($args[1]) ? $args[1] : LIBXML_NOCDATA);
				if (@$args[0] !== null)
					$result = $args[0] ? json_decode(json_encode($result), true) : json_decode(json_encode($result));
				break;
			case 'all':
				$result = isset($args[0]) ? @$info[$args[0]] : $info;
				break;
			case 'redirect_url':
				$result = @$info['redirect_url'];
				break;
		}

		return $result;
	}

	private function array_combine_recursive ($keys, $values) {
		$arr = [];
		foreach ($keys as $i => $key) {
			$value = isset($values[$i]) ? $values[$i] : null;
			if (!isset($arr[$key]))
				$arr[$key] = $value;
			else if (is_array($arr[$key]))
				$arr[$key][] = $value;
			else
				$arr[$key] = [$arr[$key], $value];
		}

		return $arr;
	}

	private function parse_cookie ($header) {
		$parse_cookie_str = function ($str) {
			$p      = preg_match_all('/(?<=^|;)\s*([^=]+?)\s*=\s*(.*?)\s*(?=;|$)/', $str, $m);
			$i      = 0;
			$cookie = [];
			while ($i < $p) {
				if ($i === 0) {
					$cookie['key']   = $m[1][$i];
					$cookie['value'] = $m[2][$i];
				} else if ($m[1][$i] === 'expires') {
					$d         = date_create($m[2][$i], timezone_open("Asia/Shanghai"));
					$timestamp = $d->getTimestamp();

					$cookie[$m[1][$i]] = $timestamp;
				} else
					$cookie[$m[1][$i]] = $m[2][$i];
				$i++;
			}

			return $cookie;
		};

		$cookies = [];
		if (is_array($header)) {
			foreach ($header as $cookie) {
				$cookie = $parse_cookie_str($cookie);
				if (!empty($cookie))
					$cookies[$cookie['key']] = $cookie;
			}
		} else if (is_string($header)) {
			$p = preg_match_all('/(?<=^|[\r\n])\s*Set-Cookie\s*:\s*(.+?)(?=[\r\n]|$)/i', $header, $m);
			$i = 0;
			while ($i < $p) {
				$cookie = $parse_cookie_str($m[1][$i]);
				if (!empty($cookie))
					$cookies[$cookie['key']] = $cookie;
				$i++;
			}
		}

		return $cookies;
	}

	/**
	 * 自动编码转换
	 *
	 * @param  string  $str
	 * @param  string  $charset
	 *
	 * @return string
	 */
	function charset ($str, $charset = 'UTF-8') {
		$encode = mb_detect_encoding($str, ["ASCII", "GB2312", "GBK", 'UTF-8', 'BIG5']);
		if ($encode !== false && $encode !== strtoupper($charset))
			$str = mb_convert_encoding($str, $charset, $encode);

		return $str;
	}
}